import pytest
from selenium import webdriver
from selenium.webdriver.chrome.options import Options as chrome_options
import logging
import threading





def get_chrome_options():
    options = chrome_options()
    options.add_argument("headless")  # headless or chrome
    options.add_argument("--start-maximized")
    options.add_argument("--window-size=1920,1080")
    options.add_argument('--disable-gpu')
    options.add_argument('--no-sandbox')
    options.add_argument('--disable-setuid-sandbox')
    options.add_argument('--disable-dev-shm-usage')
    return options



@pytest.fixture(scope="function")
def driver(request):
    options = get_chrome_options()
    # remote_url = "http://selenium__standalone-chrome:4444/wd/hub"
    # driver = webdriver.Remote(command_executor=remote_url, options=options)
    driver = webdriver.Chrome(options=options)
    request.cls.driver = driver
    yield driver
    driver.quit()
