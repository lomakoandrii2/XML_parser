from base import SeleniumBase
from selenium.webdriver.common.by import By


class FindLinks(SeleniumBase):
    def is_element_visible(self):
        return self.is_visible(By.CLASS_NAME, "fancybox-skin")
