import pytest
from selenium.common import TimeoutException
from my_parser import Links
from find_links import FindLinks


class TestFindLinks:

    @pytest.mark.parametrize("link", Links.links())
    def test_links(self, driver, link):
        # Ваш тест
        page = FindLinks(driver, link)
        page.open()
        try:
            page.is_element_visible()
            with open('first.txt', 'a') as file:  # Открываем файл для дополнения (append)
                file.write(driver.current_url + '\n')
        except TimeoutException:
            pass

    # @pytest.mark.parametrize("link", Links.modified_urls())
    # def test_modified_links(self, driver, link):
    #     page = FindLinks(driver, link)
    #     page.open()
    #     try:
    #         page.is_element_visible()
    #     except TimeoutException:
    #         with open('failed_links.txt', 'a') as file:
    #             file.write(driver.current_url + '\n')

