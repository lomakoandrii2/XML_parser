from selenium.common import TimeoutException
from selenium.webdriver.remote.webelement import WebElement
from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.by import By


class SeleniumBase:
    def __init__(self, driver, url):
        self.driver = driver
        self.url = url
        self.wait = WebDriverWait(self.driver, 6, 0.3)

    def open(self):
        self.driver.get(self.url)

    def is_visible(self, how, what) -> WebElement:
        return self.wait.until(EC.visibility_of_element_located((how, what)))