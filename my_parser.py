import xml.etree.ElementTree as ET


class Links:
    @staticmethod
    def links():
        # Загрузка XML-файла
        tree = ET.parse('import.aut1.stage.interactivated.me_pub_sitemap_it.xml')
        root = tree.getroot()

        # Поиск всех ссылок
        links = []
        for url in root.iter('{http://www.sitemaps.org/schemas/sitemap/0.9}url'):
            loc = url.find('{http://www.sitemaps.org/schemas/sitemap/0.9}loc').text
            links.append(loc)
        return links

    @staticmethod
    def modified_urls():
        with open('first.txt', 'r') as f:
            urls = f.readlines()
        modified_urls = [url.replace('/it/', '/en/') for url in urls]
        return modified_urls


